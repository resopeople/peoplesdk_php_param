<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/param/library/ConstParam.php');
include($strRootPath . '/src/param/library/ToolBoxParam.php');
include($strRootPath . '/src/param/model/ParamEntity.php');
include($strRootPath . '/src/param/model/ParamEntityFactory.php');
include($strRootPath . '/src/param/model/repository/ParamEntitySimpleRepository.php');