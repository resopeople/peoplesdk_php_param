<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\param\param\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\param\param\model\ParamEntity;
use people_sdk\param\param\model\repository\ParamEntitySimpleRepository;



class ToolBoxParam extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of scoped attribute keys,
     * from specified scoped attribute save names.
     *
     * @param ParamEntity $objParamEntity
     * @param array $tabAttrNmSave
     * @return null|array
     */
    public static function getTabScopeAttrKey(
        ParamEntity $objParamEntity,
        array $tabAttrNmSave
    )
    {
        // Init var
        $tabAttrNmSave = array_filter(
            $tabAttrNmSave,
            function($strAttrNmSave) {return is_string($strAttrNmSave);}
        );
        $result = (
            (!is_null($tabAttrNmSave)) ?
                array_values(array_filter(
                    $objParamEntity->getTabAttributeKey(),
                    function($strAttrKey) use ($objParamEntity, $tabAttrNmSave) {
                        return in_array(
                            $objParamEntity->getAttributeNameSave($strAttrKey),
                            $tabAttrNmSave
                        );
                    }
                )) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of scoped attribute keys,
     * for scope get.
     *
     * Param entity simple repository execution configuration array format:
     * @see ParamEntitySimpleRepository::getTabScopeGetAttrNameSave() configuration array format.
     *
     * @param ParamEntitySimpleRepository $objParamEntitySimpleRepository
     * @param ParamEntity $objParamEntity
     * @param null|array $tabParamEntitySimpleRepoExecConfig = null
     * @return null|array
     */
    public static function getTabScopeGetAttrKey(
        ParamEntitySimpleRepository $objParamEntitySimpleRepository,
        ParamEntity $objParamEntity,
        array $tabParamEntitySimpleRepoExecConfig = null
    )
    {
        // Return result
        return (
            (!is_null($tabAttrNmSave =
                $objParamEntitySimpleRepository->getTabScopeGetAttrNameSave($tabParamEntitySimpleRepoExecConfig))) ?
                static::getTabScopeAttrKey($objParamEntity, $tabAttrNmSave) :
                null
        );
    }



    /**
     * Get index array of scoped attribute keys,
     * for scope update.
     *
     * Param entity simple repository execution configuration array format:
     * @see ParamEntitySimpleRepository::getTabScopeUpdateAttrNameSave() configuration array format.
     *
     * @param ParamEntitySimpleRepository $objParamEntitySimpleRepository
     * @param ParamEntity $objParamEntity
     * @param null|array $tabParamEntitySimpleRepoExecConfig = null
     * @return null|array
     */
    public static function getTabScopeUpdateAttrKey(
        ParamEntitySimpleRepository $objParamEntitySimpleRepository,
        ParamEntity $objParamEntity,
        array $tabParamEntitySimpleRepoExecConfig = null
    )
    {
        // Return result
        return (
            (!is_null($tabAttrNmSave =
                $objParamEntitySimpleRepository->getTabScopeUpdateAttrNameSave($tabParamEntitySimpleRepoExecConfig))) ?
                static::getTabScopeAttrKey($objParamEntity, $tabAttrNmSave) :
                null
        );
    }



}