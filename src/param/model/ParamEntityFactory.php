<?php
/**
 * This class allows to define param entity factory class.
 * Param entity factory allows to provide new param entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\param\param\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use people_sdk\param\param\model\ParamEntity;



/**
 * @method ParamEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class ParamEntityFactory extends FixEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => ParamEntity::class
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $result = new ParamEntity(
            array(),
            $objValidator
        );

        // Return result
        return $result;
    }



}