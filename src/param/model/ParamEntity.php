<?php
/**
 * This class allows to define param entity class.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\param\param\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\null_value\library\ConstNullValue;
use people_sdk\library\model\entity\null_value\library\ToolBoxNullValue;
use people_sdk\param\param\library\ConstParam;



/**
 * @property null|boolean $boolSpaceMacroAccess
 * @property null|boolean $boolSpaceAccess
 * @property null|string|integer $intSpaceAuthKeyCountMax
 * @property null|boolean $boolSpaceAuthKeyConnect
 * @property null|boolean $boolSpaceAuthKeyAdminConnect
 * @property null|string|integer $intRoleCountMax
 * @property null|array $tabAttrTzListName
 * @property null|string $strAttrDefaultGetTzName
 * @property null|string $strAttrDefaultGetDtFormat
 * @property null|string $strAttrDefaultSetTzName
 * @property null|string|array $attrDefaultSetDtFormat
 * @property null|string|integer $intDataFileSizeSumMax
 * @property null|string|integer $intUserProfileAttrCountMax
 * @property null|string|integer $intUserProfileCountMax
 * @property null|string $strUserProfilePwDefault
 * @property null|string|integer $intUserProfileImgSizeSumMax
 * @property null|string|integer $intUserProfileImgSizeSumLimit
 * @property null|string|integer $intUserProfileImgSizeLimit
 * @property null|array $tabUserProfilePermDefault
 * @property null|array $tabUserProfileRoleDefault
 * @property null|string|integer $intUserProfileApiKeyCountProfileMax
 * @property null|string|integer $intUserProfileTokenKeyCountProfileMax
 * @property null|string|integer $intUserProfileTokenKeyTimeValidStartMax
 * @property null|string|integer $intUserProfileTokenKeyTimeValidEndMin
 * @property null|string|integer $intUserProfileTokenKeyTimeValidEndMax
 * @property null|boolean $boolUserProfileConnect
 * @property null|boolean $boolUserProfileGet
 * @property null|boolean $boolUserProfileCreate
 * @property null|boolean $boolUserProfileUpdate
 * @property null|boolean $boolUserProfileDelete
 * @property null|boolean $boolUserProfilePwUpdate
 * @property null|boolean $boolUserProfileUserGet
 * @property null|boolean $boolUserProfileApiKeyGet
 * @property null|boolean $boolUserProfileApiKeyCreate
 * @property null|boolean $boolUserProfileApiKeyUpdate
 * @property null|boolean $boolUserProfileApiKeyDelete
 * @property null|boolean $boolUserProfileApiKeyKeyUpdate
 * @property null|boolean $boolUserProfileApiKeyCurrentGet
 * @property null|boolean $boolUserProfileApiKeyCurrentUpdate
 * @property null|boolean $boolUserProfileApiKeyCurrentDelete
 * @property null|boolean $boolUserProfileApiKeyCurrentKeyUpdate
 * @property null|boolean $boolUserProfileTokenKeyGet
 * @property null|boolean $boolUserProfileTokenKeyCreate
 * @property null|boolean $boolUserProfileTokenKeyUpdate
 * @property null|boolean $boolUserProfileTokenKeyDelete
 * @property null|boolean $boolUserProfileTokenKeyKeyUpdate
 * @property null|boolean $boolUserProfileTokenKeyCurrentGet
 * @property null|boolean boolUserProfileTokenKeyCurrentCreate
 * @property null|boolean $boolUserProfileTokenKeyCurrentUpdate
 * @property null|boolean $boolUserProfileTokenKeyCurrentDelete
 * @property null|boolean $boolUserProfileTokenKeyCurrentKeyUpdate
 * @property null|string|integer $intAppProfileAttrCountMax
 * @property null|string|integer $intAppProfileCountMax
 * @property null|string $strAppProfileSecretDefault
 * @property null|string|integer $intAppProfileImgSizeSumMax
 * @property null|string|integer $intAppProfileImgSizeSumLimit
 * @property null|string|integer $intAppProfileImgSizeLimit
 * @property null|array $tabAppProfilePermDefault
 * @property null|array $tabAppProfileRoleDefault
 * @property null|string|integer $intAppProfileApiKeyCountProfileMax
 * @property null|string|integer $intAppProfileTokenKeyCountProfileMax
 * @property null|string|integer $intAppProfileTokenKeyTimeValidStartMax
 * @property null|string|integer $intAppProfileTokenKeyTimeValidEndMin
 * @property null|string|integer $intAppProfileTokenKeyTimeValidEndMax
 * @property null|boolean $boolAppProfileConnect
 * @property null|boolean $boolAppProfileGet
 * @property null|boolean $boolAppProfileCreate
 * @property null|boolean $boolAppProfileUpdate
 * @property null|boolean $boolAppProfileDelete
 * @property null|boolean $boolAppProfileSecretUpdate
 * @property null|boolean $boolAppProfileApiKeyGet
 * @property null|boolean $boolAppProfileApiKeyCreate
 * @property null|boolean $boolAppProfileApiKeyUpdate
 * @property null|boolean $boolAppProfileApiKeyDelete
 * @property null|boolean $boolAppProfileApiKeyKeyUpdate
 * @property null|boolean $boolAppProfileApiKeyCurrentGet
 * @property null|boolean $boolAppProfileApiKeyCurrentUpdate
 * @property null|boolean $boolAppProfileApiKeyCurrentDelete
 * @property null|boolean $boolAppProfileApiKeyCurrentKeyUpdate
 * @property null|boolean $boolAppProfileTokenKeyGet
 * @property null|boolean $boolAppProfileTokenKeyCreate
 * @property null|boolean $boolAppProfileTokenKeyUpdate
 * @property null|boolean $boolAppProfileTokenKeyDelete
 * @property null|boolean $boolAppProfileTokenKeyKeyUpdate
 * @property null|boolean $boolAppProfileTokenKeyCurrentGet
 * @property null|boolean boolAppProfileTokenKeyCurrentCreate
 * @property null|boolean $boolAppProfileTokenKeyCurrentUpdate
 * @property null|boolean $boolAppProfileTokenKeyCurrentDelete
 * @property null|boolean $boolAppProfileTokenKeyCurrentKeyUpdate
 * @property null|boolean $boolGroupMacroAccess
 * @property null|string|integer $intGroupCountMax
 * @property null|array $tabUserProfileGroupMemberDefault
 * @property null|array $tabUserProfileGroupPermScopeDefault
 * @property null|array $tabAppProfileGroupPermScopeDefault
 * @property null|string|array $tabGroupPermScopeIncludePermDefault
 * @property null|string|array $tabGroupPermScopeExcludePermDefault
 * @property null|boolean $boolGroupGet
 * @property null|boolean $boolGroupCreate
 * @property null|boolean $boolGroupCreateMemberProfileCreate
 * @property null|boolean $boolGroupCreatePermScopeCreate
 * @property null|boolean $boolGroupMemberGet
 * @property null|boolean $boolGroupMemberProfileGet
 * @property null|boolean $boolGroupMemberProfileCreate
 * @property null|boolean $boolGroupMemberProfileDelete
 * @property null|boolean $boolGroupRelationGet
 * @property null|boolean $boolGroupRelationCreate
 * @property null|boolean $boolGroupRelationUpdate
 * @property null|boolean $boolGroupRelationDelete
 * @property null|boolean $boolFileMacroAccess
 * @property null|boolean $boolFileAccess
 * @property null|string|integer $intFileCountMax
 * @property null|string|integer $intFileSizeSumMax
 * @property null|string|integer $intFileSizeSumLimit
 * @property null|string|integer $intFileSizeLimit
 * @property null|boolean $boolFileGet
 * @property null|boolean $boolFileCreate
 * @property null|boolean $boolFileUpdate
 * @property null|boolean $boolFileDelete
 * @property null|boolean $boolFileContentGet
 * @property null|boolean $boolFileMemberGet
 * @property null|boolean $boolFileMemberCreate
 * @property null|boolean $boolFileMemberUpdate
 * @property null|boolean $boolFileMemberDelete
 * @property null|boolean $boolFileMemberCurrent
 * @property null|string|array $tabFileMemberGroup
 * @property null|string|array $tabFileMemberPublicGroup
 * @property null|boolean boolFileGrpMemberGet
 * @property null|boolean $boolFileGrpMemberCreate
 * @property null|boolean $boolFileGrpMemberUpdate
 * @property null|boolean $boolFileGrpMemberDelete
 * @property null|boolean $boolFileGrpMemberPublic
 * @property null|string|array $tabFileGrpMemberIncludeGroup
 * @property null|string|array $tabFileGrpMemberExcludeGroup
 * @property null|boolean boolItemMacroAccess
 * @property null|boolean boolItemAccess
 * @property null|string|integer intItemSchemaCountMax
 * @property null|string|integer intItemSchemaAttrCountSchemaMax
 * @property null|string|integer intItemSchemaRelationCountSchemaMax
 * @property null|string|integer intItemCategoryCountMax
 * @property null|string|integer intItemAreaCountMax
 * @property null|array tabUserProfileItemAreaMemberDefaultArea
 * @property null|string|integer intItemCountMax
 * @property null|boolean boolItemGet
 * @property null|boolean boolItemCreate
 * @property null|boolean boolItemUpdate
 * @property null|boolean oolItemDelete
 * @property null|boolean boolItemItemRelationGet
 * @property null|boolean boolItemItemRelationCreate
 * @property null|boolean boolItemItemRelationUpdate
 * @property null|boolean boolItemItemRelationDelete
 * @property null|boolean boolItemItemCategoryGet
 * @property null|boolean boolItemItemCategoryCreate
 * @property null|boolean boolItemItemCategoryUpdate
 * @property null|boolean boolItemItemCategoryDelete
 */
class ParamEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute space macro connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_SPACE_MACRO_ACCESS,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_SPACE_MACRO_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_SPACE_MACRO_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute space connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_SPACE_ACCESS,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_SPACE_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_SPACE_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute space authentication key count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_SPACE_AUTH_KEY_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_SPACE_AUTH_KEY_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute space authentication key connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_CONNECT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_SPACE_AUTH_KEY_CONNECT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_SPACE_AUTH_KEY_CONNECT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute space authentication key administrator connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_ADMIN_CONNECT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_SPACE_AUTH_KEY_ADMIN_CONNECT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_SPACE_AUTH_KEY_ADMIN_CONNECT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute role count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ROLE_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ROLE_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ROLE_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute timezone list names
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_TIMEZONE_LIST_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_TIMEZONE_LIST_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_TIMEZONE_LIST_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute default get timezone name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_DEFAULT_GET_TIMEZONE_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_DEFAULT_GET_TIMEZONE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_DEFAULT_GET_TIMEZONE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute default get datetime format
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_DEFAULT_GET_DATETIME_FORMAT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_DEFAULT_GET_DATETIME_FORMAT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_DEFAULT_GET_DATETIME_FORMAT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute default set timezone name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_DEFAULT_SET_TIMEZONE_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_DEFAULT_SET_TIMEZONE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_DEFAULT_SET_TIMEZONE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute default set datetime format
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_DEFAULT_SET_DATETIME_FORMAT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_DEFAULT_SET_DATETIME_FORMAT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_DEFAULT_SET_DATETIME_FORMAT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute data file size sum maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_DATA_FILE_SIZE_SUM_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_DATA_FILE_SIZE_SUM_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_DATA_FILE_SIZE_SUM_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile attribute count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ATTR_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_ATTR_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_ATTR_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile password default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PW_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_PW_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_PW_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile image size sum maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_IMG_SIZE_SUM_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_IMG_SIZE_SUM_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile image size sum limitation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_LIMIT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_IMG_SIZE_SUM_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_IMG_SIZE_SUM_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile image size limitation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_LIMIT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_IMG_SIZE_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_IMG_SIZE_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile permission default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PERM_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_PERM_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_PERM_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile role default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ROLE_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_ROLE_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_ROLE_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile api key count, per profile, maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_COUNT_PROFILE_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_COUNT_PROFILE_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_COUNT_PROFILE_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key count, per profile, maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key time (seconds) validity start maximum
            // (from token key datetime create)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key time (seconds) validity end minimum
            // (from token key datetime validity start)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key time (seconds) validity end maximum
            // (from token key datetime validity start)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_CONNECT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_CONNECT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_CONNECT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile password modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PW_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_PW_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_PW_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile user profile read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_USER_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_USER_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_USER_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile API key read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile API key creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile API key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile API key removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile API key key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile API key current read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_CURRENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_CURRENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile API key current modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_CURRENT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_CURRENT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile API key current removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_CURRENT_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_CURRENT_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile API key current key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_API_KEY_CURRENT_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_API_KEY_CURRENT_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key current read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_CURRENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_CURRENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key current creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_CURRENT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_CURRENT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key current modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_CURRENT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_CURRENT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key current removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_CURRENT_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_CURRENT_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile token key current key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile attribute count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_ATTR_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_ATTR_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_ATTR_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile secret default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_SECRET_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_SECRET_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_SECRET_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile image size sum maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_IMG_SIZE_SUM_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_IMG_SIZE_SUM_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile image size sum limitation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_LIMIT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_IMG_SIZE_SUM_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_IMG_SIZE_SUM_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile image size limitation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_LIMIT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_IMG_SIZE_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_IMG_SIZE_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile permission default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_PERM_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_PERM_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_PERM_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile role default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_ROLE_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_ROLE_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_ROLE_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile api key count, per profile, maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_COUNT_PROFILE_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_COUNT_PROFILE_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_COUNT_PROFILE_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key count, per profile, maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key time (seconds) validity start maximum
            // (from token key datetime create)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key time (seconds) validity end minimum
            // (from token key datetime validity start)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key time (seconds) validity end maximum
            // (from token key datetime validity start)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_CONNECT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_CONNECT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_CONNECT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile secret modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_SECRET_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_SECRET_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_SECRET_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile API key read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile API key creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile API key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile API key removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile API key key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile API key current read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_CURRENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_CURRENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile API key current modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_CURRENT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_CURRENT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile API key current removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_CURRENT_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_CURRENT_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile API key current key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_API_KEY_CURRENT_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_API_KEY_CURRENT_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key current read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_CURRENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_CURRENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key current creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_CURRENT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_CURRENT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key current modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_CURRENT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_CURRENT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key current removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_CURRENT_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_CURRENT_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile token key current key modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group macro connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_MACRO_ACCESS,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_MACRO_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_MACRO_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile group default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_GROUP_MEMBER_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_GROUP_MEMBER_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_GROUP_MEMBER_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile group permission scope default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_GROUP_PERM_SCOPE_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_GROUP_PERM_SCOPE_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_GROUP_PERM_SCOPE_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile group permission scope default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_APP_PROFILE_GROUP_PERM_SCOPE_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_APP_PROFILE_GROUP_PERM_SCOPE_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_APP_PROFILE_GROUP_PERM_SCOPE_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group permission scope included permission default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_INCLUDE_PERM_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_PERM_SCOPE_INCLUDE_PERM_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_PERM_SCOPE_INCLUDE_PERM_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group permission scope excluded permission default
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_EXCLUDE_PERM_DEFAULT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_PERM_SCOPE_EXCLUDE_PERM_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_PERM_SCOPE_EXCLUDE_PERM_DEFAULT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile member creation, on group creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_CREATE_MEMBER_PROFILE_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_CREATE_MEMBER_PROFILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_CREATE_MEMBER_PROFILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute permission scope creation, on group creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_CREATE_PERM_SCOPE_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_CREATE_PERM_SCOPE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_CREATE_PERM_SCOPE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute member read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_MEMBER_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_MEMBER_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile member read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_PROFILE_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_MEMBER_PROFILE_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_MEMBER_PROFILE_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile member creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_PROFILE_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_MEMBER_PROFILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_MEMBER_PROFILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile member removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_PROFILE_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_MEMBER_PROFILE_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_MEMBER_PROFILE_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_RELATION_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_RELATION_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_RELATION_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_RELATION_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_RELATION_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_RELATION_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_GROUP_RELATION_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_GROUP_RELATION_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file macro connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_MACRO_ACCESS,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_MACRO_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_MACRO_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_ACCESS,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file size sum maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_SIZE_SUM_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_SIZE_SUM_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file size sum limitation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_LIMIT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_SIZE_SUM_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_SIZE_SUM_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file size limitation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_SIZE_LIMIT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_SIZE_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_SIZE_LIMIT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file content read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_CONTENT_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_CONTENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_CONTENT_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute member read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_MEMBER_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_MEMBER_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute member creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_MEMBER_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_MEMBER_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute member modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_MEMBER_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_MEMBER_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute member removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_MEMBER_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_MEMBER_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute member current (current user profile available, if possible)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_CURRENT,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_MEMBER_CURRENT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_MEMBER_CURRENT,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute member groups (list of valid related user profiles available)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_GROUP,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_MEMBER_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_MEMBER_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute member public groups (list of valid member user profiles available)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_PUBLIC_GROUP,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_MEMBER_PUBLIC_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_MEMBER_PUBLIC_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group member read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_GRP_MEMBER_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_GRP_MEMBER_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group member creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_GRP_MEMBER_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_GRP_MEMBER_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group member modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_GRP_MEMBER_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_GRP_MEMBER_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group member removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_GRP_MEMBER_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_GRP_MEMBER_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group member public enable
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_PUBLIC,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_GRP_MEMBER_PUBLIC,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_GRP_MEMBER_PUBLIC,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group member included groups
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_INCLUDE_GROUP,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_GRP_MEMBER_INCLUDE_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_GRP_MEMBER_INCLUDE_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group member excluded groups
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_EXCLUDE_GROUP,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_FILE_GRP_MEMBER_EXCLUDE_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_FILE_GRP_MEMBER_EXCLUDE_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item macro connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_MACRO_ACCESS,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_MACRO_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_MACRO_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item connection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_ACCESS,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_ACCESS,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute schema count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_SCHEMA_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_SCHEMA_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute schema attribute count, per schema, maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_ATTR_COUNT_SCHEMA_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_SCHEMA_ATTR_COUNT_SCHEMA_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_SCHEMA_ATTR_COUNT_SCHEMA_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute schema relation count, per schema, maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_RELATION_COUNT_SCHEMA_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_SCHEMA_RELATION_COUNT_SCHEMA_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_SCHEMA_RELATION_COUNT_SCHEMA_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute category count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_CATEGORY_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_CATEGORY_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_CATEGORY_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute area count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_AREA_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_AREA_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_AREA_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile area member default area
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ITEM_AREA_MEMBER_DEFAULT_AREA,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_USER_PROFILE_ITEM_AREA_MEMBER_DEFAULT_AREA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_USER_PROFILE_ITEM_AREA_MEMBER_DEFAULT_AREA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item count maximum
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_COUNT_MAX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_COUNT_MAX,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item relation read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_ITEM_RELATION_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_ITEM_RELATION_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item relation creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_ITEM_RELATION_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_ITEM_RELATION_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item relation modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_ITEM_RELATION_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_ITEM_RELATION_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item relation removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_ITEM_RELATION_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_ITEM_RELATION_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item category read
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_GET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_ITEM_CATEGORY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_ITEM_CATEGORY_GET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item category creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_ITEM_CATEGORY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_ITEM_CATEGORY_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item category modification
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_ITEM_CATEGORY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_ITEM_CATEGORY_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item category removing
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_DELETE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstParam::ATTRIBUTE_ALIAS_ITEM_ITEM_CATEGORY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstParam::ATTRIBUTE_NAME_SAVE_ITEM_ITEM_CATEGORY_DELETE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $getTabRuleConfigValidInteger = function(
            $boolCompareEqualRequire = true,
            $boolNullRequire = true,
            $boolValueNullRequire = true
        )
        {
            $boolCompareEqualRequire = (is_bool($boolCompareEqualRequire) ? $boolCompareEqualRequire : true);
            $boolNullRequire = (is_bool($boolNullRequire) ? $boolNullRequire : true);
            $boolValueNullRequire = (is_bool($boolValueNullRequire) ? $boolValueNullRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => $boolCompareEqualRequire
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' =>
                            '%1$s must be a ' .
                            ($boolCompareEqualRequire ? '' : 'strict ') .
                            'positive integer.'
                    ]
                ]
            );

            if($boolNullRequire || $boolValueNullRequire)
            {
                if($boolNullRequire)
                {
                    $result[0][1]['rule_config']['is-null'] = array('is_null');
                }

                if($boolValueNullRequire)
                {
                    $result[0][1]['rule_config']['is-valid-null'] = array(
                        [
                            'compare_equal',
                            ['compare_value' => ConstNullValue::NULL_VALUE]
                        ]
                    );
                }

                $result[0][1]['error_message_pattern'] =
                    '%1$s must be null or a ' .
                    ($boolCompareEqualRequire ? '' : 'strict ') .
                    'positive integer.';
            }

            return $result;
        };

        $getTabRuleConfigValidString = function(
            $boolNullRequire = true,
            $boolValueNullRequire = true
        )
        {
            $boolNullRequire = (is_bool($boolNullRequire) ? $boolNullRequire : true);
            $boolValueNullRequire = (is_bool($boolValueNullRequire) ? $boolValueNullRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-valid-string' => [
                                'type_string',
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty']
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a string, not empty'
                    ]
                ]
            );

            if($boolNullRequire || $boolValueNullRequire)
            {
                if($boolNullRequire)
                {
                    $result[0][1]['rule_config']['is-null'] = array('is_null');
                }

                if($boolValueNullRequire)
                {
                    $result[0][1]['rule_config']['is-valid-null'] = array(
                        [
                            'compare_equal',
                            ['compare_value' => ConstNullValue::NULL_VALUE]
                        ]
                    );
                }

                $result[0][1]['error_message_pattern'] = '%1$s must be null or a string, not empty';
            }

            return $result;
        };

        $getTabRuleConfigValidTabString = function(
            $boolNullRequire = true,
            $boolValueNullRequire = true
        ) use (
            $getTabRuleConfigValidString
        )
        {
            $boolNullRequire = (is_bool($boolNullRequire) ? $boolNullRequire : true);
            $boolValueNullRequire = (is_bool($boolValueNullRequire) ? $boolValueNullRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-valid-iterate-string' => [
                                [
                                    'type_array',
                                    [
                                        'index_only_require' => true
                                    ]
                                ],
                                [
                                    'sub_rule_iterate',
                                    [
                                        'rule_config' => $getTabRuleConfigValidString(
                                            false,
                                            false
                                        )
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid array of strings, not empty'
                    ]
                ]
            );

            if($boolNullRequire || $boolValueNullRequire)
            {
                if($boolNullRequire)
                {
                    $result[0][1]['rule_config']['is-null'] = array('is_null');
                }

                if($boolValueNullRequire)
                {
                    $result[0][1]['rule_config']['is-valid-null'] = array(
                        [
                            'compare_equal',
                            ['compare_value' => ConstNullValue::NULL_VALUE]
                        ]
                    );
                }

                $result[0][1]['error_message_pattern'] = '%1$s must be null or a valid array of strings, not empty';
            }

            return $result;
        };

        $getTabRuleConfigValidTabIdString = function(
            $boolNullRequire = true,
            $boolValueNullRequire = true
        ) use (
            $getTabRuleConfigValidInteger,
            $getTabRuleConfigValidString
        )
        {
            $boolNullRequire = (is_bool($boolNullRequire) ? $boolNullRequire : true);
            $boolValueNullRequire = (is_bool($boolValueNullRequire) ? $boolValueNullRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-valid-iterate-id-string' => [
                                [
                                    'type_array',
                                    [
                                        'index_only_require' => true
                                    ]
                                ],
                                [
                                    'sub_rule_iterate',
                                    [
                                        'rule_config' => [
                                            [
                                                'group_sub_rule_or',
                                                [
                                                    'rule_config' => [
                                                        'is-valid-id' => $getTabRuleConfigValidInteger(
                                                            false,
                                                            false,
                                                            false
                                                        ),
                                                        'is-valid-string' => $getTabRuleConfigValidString(
                                                            false,
                                                            false
                                                        )
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid array of strings/IDs.'
                    ]
                ]
            );

            if($boolNullRequire || $boolValueNullRequire)
            {
                if($boolNullRequire)
                {
                    $result[0][1]['rule_config']['is-null'] = array('is_null');
                }

                if($boolValueNullRequire)
                {
                    $result[0][1]['rule_config']['is-valid-null'] = array(
                        [
                            'compare_equal',
                            ['compare_value' => ConstNullValue::NULL_VALUE]
                        ]
                    );
                }

                $result[0][1]['error_message_pattern'] = '%1$s must be null or a valid array of strings/IDs.';
            }

            return $result;
        };

        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstParam::ATTRIBUTE_KEY_SPACE_MACRO_ACCESS => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_SPACE_ACCESS => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_CONNECT => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_ADMIN_CONNECT => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ROLE_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_TIMEZONE_LIST_NAME => $getTabRuleConfigValidTabString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_DEFAULT_GET_TIMEZONE_NAME => $getTabRuleConfigValidString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_DEFAULT_GET_DATETIME_FORMAT => $getTabRuleConfigValidString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_DEFAULT_SET_TIMEZONE_NAME => $getTabRuleConfigValidString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_DEFAULT_SET_DATETIME_FORMAT => array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-string' => $getTabRuleConfigValidString(
                                false,
                                false
                            ),
                            'is-valid-iterate-string' => array_merge(
                                $getTabRuleConfigValidTabString(
                                    false,
                                    false
                                ),
                                [
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => ['is_empty']
                                        ]
                                    ]
                                ]
                            )
                        ],
                        'error_message_pattern' => '%1$s must be null or a string/array of strings.'
                    ]
                ]
            ),
            ConstParam::ATTRIBUTE_KEY_DATA_FILE_SIZE_SUM_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ATTR_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PW_DEFAULT => $getTabRuleConfigValidString(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_LIMIT => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_LIMIT => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PERM_DEFAULT => $getTabRuleConfigValidTabString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ROLE_DEFAULT => $getTabRuleConfigValidTabIdString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_COUNT_PROFILE_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN => $getTabRuleConfigValidInteger(false),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX => $getTabRuleConfigValidInteger(false),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_CONNECT => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PW_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_USER_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_ATTR_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_SECRET_DEFAULT => $getTabRuleConfigValidString(),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_LIMIT => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_LIMIT => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_PERM_DEFAULT => $getTabRuleConfigValidTabString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_ROLE_DEFAULT => $getTabRuleConfigValidTabIdString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_COUNT_PROFILE_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN => $getTabRuleConfigValidInteger(false),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX => $getTabRuleConfigValidInteger(false),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_CONNECT => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_SECRET_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_MACRO_ACCESS => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_GROUP_MEMBER_DEFAULT => $getTabRuleConfigValidTabIdString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_GROUP_PERM_SCOPE_DEFAULT => $getTabRuleConfigValidTabIdString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_APP_PROFILE_GROUP_PERM_SCOPE_DEFAULT => $getTabRuleConfigValidTabIdString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_INCLUDE_PERM_DEFAULT => $getTabRuleConfigValidTabString(),
            ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_EXCLUDE_PERM_DEFAULT => $getTabRuleConfigValidTabString(),
            ConstParam::ATTRIBUTE_KEY_GROUP_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_CREATE_MEMBER_PROFILE_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_CREATE_PERM_SCOPE_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_PROFILE_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_PROFILE_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_PROFILE_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_MACRO_ACCESS => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_ACCESS => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_LIMIT => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_FILE_SIZE_LIMIT => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_FILE_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_CONTENT_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_CURRENT => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_GROUP => $getTabRuleConfigValidTabIdString(),
            ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_PUBLIC_GROUP => $getTabRuleConfigValidTabIdString(),
            ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_PUBLIC => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_INCLUDE_GROUP => $getTabRuleConfigValidTabIdString(),
            ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_EXCLUDE_GROUP => $getTabRuleConfigValidTabIdString(),
            ConstParam::ATTRIBUTE_KEY_ITEM_MACRO_ACCESS => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_ACCESS => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_ATTR_COUNT_SCHEMA_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_RELATION_COUNT_SCHEMA_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_ITEM_CATEGORY_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_ITEM_AREA_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ITEM_AREA_MEMBER_DEFAULT_AREA => $getTabRuleConfigValidTabIdString(
                true,
                false
            ),
            ConstParam::ATTRIBUTE_KEY_ITEM_COUNT_MAX => $getTabRuleConfigValidInteger(),
            ConstParam::ATTRIBUTE_KEY_ITEM_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_DELETE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_GET => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_CREATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_UPDATE => $tabRuleConfigValidBoolean,
            ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_DELETE => $tabRuleConfigValidBoolean,
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstParam::ATTRIBUTE_KEY_SPACE_MACRO_ACCESS:
            case ConstParam::ATTRIBUTE_KEY_SPACE_ACCESS:
            case ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_CONNECT:
            case ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_ADMIN_CONNECT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_CONNECT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_GET:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_CREATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_DELETE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PW_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_USER_GET:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_GET:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CREATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_DELETE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_GET:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_DELETE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_CURRENT_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_GET:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CREATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_DELETE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_GET:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_CREATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_DELETE:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_CONNECT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_GET:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_CREATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_DELETE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_SECRET_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_GET:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CREATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_DELETE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_GET:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_DELETE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_CURRENT_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_GET:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CREATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_DELETE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_GET:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_CREATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_DELETE:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_CURRENT_KEY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_GROUP_MACRO_ACCESS:
            case ConstParam::ATTRIBUTE_KEY_GROUP_GET:
            case ConstParam::ATTRIBUTE_KEY_GROUP_CREATE:
            case ConstParam::ATTRIBUTE_KEY_GROUP_CREATE_MEMBER_PROFILE_CREATE:
            case ConstParam::ATTRIBUTE_KEY_GROUP_CREATE_PERM_SCOPE_CREATE:
            case ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_GET:
            case ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_PROFILE_GET:
            case ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_PROFILE_CREATE:
            case ConstParam::ATTRIBUTE_KEY_GROUP_MEMBER_PROFILE_DELETE:
            case ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_GET:
            case ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_CREATE:
            case ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_GROUP_RELATION_DELETE:
            case ConstParam::ATTRIBUTE_KEY_FILE_MACRO_ACCESS:
            case ConstParam::ATTRIBUTE_KEY_FILE_ACCESS:
            case ConstParam::ATTRIBUTE_KEY_FILE_GET:
            case ConstParam::ATTRIBUTE_KEY_FILE_CREATE:
            case ConstParam::ATTRIBUTE_KEY_FILE_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_FILE_DELETE:
            case ConstParam::ATTRIBUTE_KEY_FILE_CONTENT_GET:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_GET:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_CREATE:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_DELETE:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_CURRENT:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_GET:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_CREATE:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_DELETE:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_PUBLIC:
            case ConstParam::ATTRIBUTE_KEY_ITEM_MACRO_ACCESS:
            case ConstParam::ATTRIBUTE_KEY_ITEM_ACCESS:
            case ConstParam::ATTRIBUTE_KEY_ITEM_GET:
            case ConstParam::ATTRIBUTE_KEY_ITEM_CREATE:
            case ConstParam::ATTRIBUTE_KEY_ITEM_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_ITEM_DELETE:
            case ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_GET:
            case ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_CREATE:
            case ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_RELATION_DELETE:
            case ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_GET:
            case ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_CREATE:
            case ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_UPDATE:
            case ConstParam::ATTRIBUTE_KEY_ITEM_ITEM_CATEGORY_DELETE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            case ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ROLE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_DATA_FILE_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ATTR_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_ATTR_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX:
            case ConstParam::ATTRIBUTE_KEY_GROUP_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_FILE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_FILE_SIZE_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_ATTR_COUNT_SCHEMA_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_RELATION_COUNT_SCHEMA_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_CATEGORY_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_AREA_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_COUNT_MAX:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ROLE_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_ROLE_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_GROUP_PERM_SCOPE_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_GROUP_PERM_SCOPE_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_GROUP:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_PUBLIC_GROUP:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_INCLUDE_GROUP:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_EXCLUDE_GROUP:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ITEM_AREA_MEMBER_DEFAULT_AREA:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_array($value) ?
                                array_values(array_map(
                                    function($value) {
                                        return ((is_string($value) && ctype_digit($value)) ? intval($value) : $value);
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstParam::ATTRIBUTE_KEY_DEFAULT_GET_TIMEZONE_NAME:
            case ConstParam::ATTRIBUTE_KEY_DEFAULT_GET_DATETIME_FORMAT:
            case ConstParam::ATTRIBUTE_KEY_DEFAULT_SET_TIMEZONE_NAME:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PW_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_SECRET_DEFAULT:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstParam::ATTRIBUTE_KEY_TIMEZONE_LIST_NAME:
            case ConstParam::ATTRIBUTE_KEY_DEFAULT_SET_DATETIME_FORMAT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PERM_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_PERM_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_GROUP_MEMBER_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_INCLUDE_PERM_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_EXCLUDE_PERM_DEFAULT:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ROLE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_DATA_FILE_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ATTR_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PW_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_ATTR_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_SECRET_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX:
            case ConstParam::ATTRIBUTE_KEY_GROUP_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_INCLUDE_PERM_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_EXCLUDE_PERM_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_FILE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_FILE_SIZE_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_GROUP:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_PUBLIC_GROUP:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_INCLUDE_GROUP:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_EXCLUDE_GROUP:
            case ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_ATTR_COUNT_SCHEMA_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_RELATION_COUNT_SCHEMA_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_CATEGORY_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_AREA_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_COUNT_MAX:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatGet($value);
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstParam::ATTRIBUTE_KEY_SPACE_AUTH_KEY_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ROLE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_DATA_FILE_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_ATTR_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_PW_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_SUM_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_IMG_SIZE_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_API_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN:
            case ConstParam::ATTRIBUTE_KEY_USER_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_ATTR_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_SECRET_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_SUM_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_IMG_SIZE_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_API_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_COUNT_PROFILE_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_START_MAX:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MIN:
            case ConstParam::ATTRIBUTE_KEY_APP_PROFILE_TOKEN_KEY_TIME_VALID_END_MAX:
            case ConstParam::ATTRIBUTE_KEY_GROUP_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_INCLUDE_PERM_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_GROUP_PERM_SCOPE_EXCLUDE_PERM_DEFAULT:
            case ConstParam::ATTRIBUTE_KEY_FILE_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_MAX:
            case ConstParam::ATTRIBUTE_KEY_FILE_SIZE_SUM_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_FILE_SIZE_LIMIT:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_GROUP:
            case ConstParam::ATTRIBUTE_KEY_FILE_MEMBER_PUBLIC_GROUP:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_INCLUDE_GROUP:
            case ConstParam::ATTRIBUTE_KEY_FILE_GRP_MEMBER_EXCLUDE_GROUP:
            case ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_ATTR_COUNT_SCHEMA_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_SCHEMA_RELATION_COUNT_SCHEMA_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_CATEGORY_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_AREA_COUNT_MAX:
            case ConstParam::ATTRIBUTE_KEY_ITEM_COUNT_MAX:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}